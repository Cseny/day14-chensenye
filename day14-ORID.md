# O

- Front and rear joint debugging.
- Cross domain issues.


# R

This is a fruitful day.

# I

- Through front-end and back-end debugging exercises, not only did I practice the recently learned front-end technologies, but I also reviewed the previously learned back-end unit testing and spring boot three-layer structure.

# D

- When calling the backend interface on the front-end, a cross domain issue was found, which is a built-in security mechanism of the browser. By creating a configuration Cors, this cross domain issue can be resolved. I found through debugging that the cross domain issue is actually just a security interception by the browser, and it is actually called to the backend interface.

