package com.afs.todoList;
import com.afs.todoList.advice.ErrorResponse;
import com.afs.todoList.entity.Todo;
import com.afs.todoList.exception.TodoNotFoundException;
import com.afs.todoList.repository.TodoRepository;
import com.afs.todoList.service.dto.TodoRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class TodoControllerTest {

    public static final String TEXT = "Text";
    public static final String UPDATE = "Update";
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TodoRepository todoRepository;

    @BeforeEach
    void setUp() {
        todoRepository.deleteAll();
    }
    

    @Test
    void should_create_todo() throws Exception {
        Todo todo = getTodo(TEXT);
        ObjectMapper objectMapper = new ObjectMapper();
        String todoRequest = objectMapper.writeValueAsString(todo);

        mockMvc.perform(post("/todoList")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(todoRequest))
                .andExpect(MockMvcResultMatchers.status().is(HttpStatus.CREATED.value()))
                .andExpect(result -> Assertions.assertEquals(todoRepository.count(), 1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(todo.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todo.getDone()));
    }

    @Test
    void should_find_todos() throws Exception {
        Todo todo1 = getTodo(TEXT);
        todoRepository.save(todo1);
        Todo todo2 = getTodo(UPDATE);
        todoRepository.save(todo2);

        mockMvc.perform(get("/todoList"))
                .andExpect(MockMvcResultMatchers.status().is(HttpStatus.OK.value()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(todo1.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].text").value(todo1.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].done").value(false));
    }

    @Test
    void should_update_todo_text() throws Exception {
        Todo previousTodo = getTodo(TEXT);
        todoRepository.save(previousTodo);
        TodoRequest updateTodoReq = new TodoRequest();
        updateTodoReq.setText(UPDATE);
        ObjectMapper objectMapper = new ObjectMapper();
        String updatedTodoJson = objectMapper.writeValueAsString(updateTodoReq);

        mockMvc.perform(put("/todoList/{id}", previousTodo.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedTodoJson))
                .andExpect(MockMvcResultMatchers.status().is(HttpStatus.OK.value()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(previousTodo.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(UPDATE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(false));
    }

    @Test
    void should_update_todo_done() throws Exception {
        Todo previousTodo = getTodo(TEXT);
        todoRepository.save(previousTodo);
        TodoRequest updateTodoReq = new TodoRequest();
        updateTodoReq.setDone(true);
        ObjectMapper objectMapper = new ObjectMapper();
        String updatedTodoJson = objectMapper.writeValueAsString(updateTodoReq);

        mockMvc.perform(put("/todoList/{id}", previousTodo.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedTodoJson))
                .andExpect(MockMvcResultMatchers.status().is(HttpStatus.OK.value()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(previousTodo.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(TEXT))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(true));
    }

    @Test
    void should_delete_todo_by_id() throws Exception {
        Todo todo = getTodo(TEXT);
        todoRepository.save(todo);

        mockMvc.perform(delete("/todoList/{id}", todo.getId()))
                .andExpect(MockMvcResultMatchers.status().is(HttpStatus.NO_CONTENT.value()));

        assertTrue(todoRepository.findById(todo.getId()).isEmpty());
    }

    @Test
    void should_find_todo_by_id() throws Exception {
        Todo todo = getTodo(TEXT);
        todoRepository.save(todo);

        mockMvc.perform(get("/todoList/{id}", todo.getId()))
                .andExpect(MockMvcResultMatchers.status().is(HttpStatus.OK.value()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(todo.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(todo.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(false));
    }

    @Test
    void should_return_error_when_find_todo_by_error_id() throws Exception {
        Todo todo = getTodo(TEXT);
        todoRepository.save(todo);

        mockMvc.perform(get("/todoList/{id}", 3L))
                .andExpect(MockMvcResultMatchers.status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("todo id not found"));
    }

    private static Todo getTodo(String text) {
        Todo todo = new Todo();
        todo.setText(text);
        todo.setDone(false);
        return todo;
    }
}