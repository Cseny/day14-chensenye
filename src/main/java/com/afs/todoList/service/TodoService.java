package com.afs.todoList.service;

import com.afs.todoList.entity.Todo;
import com.afs.todoList.exception.TodoNotFoundException;
import com.afs.todoList.repository.TodoRepository;
import com.afs.todoList.service.dto.TodoResponse;
import com.afs.todoList.service.mapper.TodoMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TodoService {

    private final TodoRepository todoRepository;

    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    public List<TodoResponse> findAll() {
        return todoRepository.findAll().stream().map(TodoMapper::toResponse).collect(Collectors.toList());
    }

    public TodoResponse update(Long id, Todo todo) {
        Todo originTodo = todoRepository.findById(id).orElseThrow();
        if (todo.getText() != null) {
            originTodo.setText(todo.getText());
        }
        if (todo.getDone() != null) {
            originTodo.setDone(todo.getDone());
        }
        return TodoMapper.toResponse(todoRepository.save(originTodo));
    }

    public TodoResponse create(Todo todo) {
        return TodoMapper.toResponse(todoRepository.save(todo));
    }

    public void delete(Long id) {
        todoRepository.deleteById(id);
    }

    public TodoResponse findById(Long id) {
        return TodoMapper.toResponse(todoRepository.findById(id).orElseThrow(TodoNotFoundException::new));
    }
}
