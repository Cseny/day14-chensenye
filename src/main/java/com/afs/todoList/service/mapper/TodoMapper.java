package com.afs.todoList.service.mapper;


import com.afs.todoList.entity.Todo;
import com.afs.todoList.service.dto.TodoRequest;
import com.afs.todoList.service.dto.TodoResponse;
import org.springframework.beans.BeanUtils;

public class TodoMapper {

    public TodoMapper() {
    }

    public static Todo toEntity(TodoRequest request) {
        Todo Todo = new Todo();
        BeanUtils.copyProperties(request, Todo);
        return Todo;
    }

    public static TodoResponse toResponse(Todo Todo) {
        TodoResponse TodoResponse = new TodoResponse();
        BeanUtils.copyProperties(Todo, TodoResponse);
        return TodoResponse;
    }
}
