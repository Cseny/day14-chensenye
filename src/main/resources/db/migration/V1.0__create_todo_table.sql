CREATE TABLE if NOT EXISTS todo (
    id BIGINT auto_increment NOT NULL primary key,
    text varchar(255) not null,
    done boolean
);